import { Module } from '@nestjs/common';
import { ProjectController } from './controllers/ProjectController';
import { ProjectService } from './services/ProjectService';
import { Project } from './enities/ProjectEntity';
import {TypeOrmModule} from '@nestjs/typeorm';
import { User } from './enities/UserEntity';
import { UserController } from './controllers/UserController';
import { UserService } from './services/UserService';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project, User]),
  ],
  controllers: [
    ProjectController,
    UserController,
  ],
  providers: [
    ProjectService,
    UserService,
  ],
})
export class DataBaseModule {
}
