import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ProjectService } from '../services/ProjectService';
import { Project } from '../enities/ProjectEntity';
import { CreateProjectDto } from '../dtos/CreateProjectDto';

@Controller('projects')
export class ProjectController {

  constructor(private projectService: ProjectService) {  }

  @Get()
  findAll(): Promise<Project[]> {
    return this.projectService.findAll();
  }

  @Get('/user/:id')
  findallByUser(@Param('id') id): Promise<Project[]> {
    return this.projectService.findAllByUser(id);
  }

  @Get(':id')
  findOne(@Param('id') id): Observable<Project> {
    return this.projectService.findOne(id);
  }

  @Post()
  create(@Body() createProjectDto: CreateProjectDto) {
    return this.projectService.create(createProjectDto);
  }

  @Put(':id')
  edit(@Param('id') id, @Body() createProjectDto: CreateProjectDto) {
    return this.projectService.edit(id, createProjectDto);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.projectService.remove(id);
  }
}
