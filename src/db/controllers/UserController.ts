import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { User } from '../enities/UserEntity';
import { UserService } from '../services/UserService';
import { CreateUserDto } from '../dtos/CreateUserDto';

@Controller('users')
export class UserController {

  constructor(private userService: UserService) {
  }

  @Get()
  findAll(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get(':email')
  findOneByEmail(@Param('email') email): Promise<User> {
    return this.userService.findOneByEmail(email);
  }

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

}
