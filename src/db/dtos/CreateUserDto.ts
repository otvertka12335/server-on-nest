export class CreateUserDto {
  readonly id: number;
  readonly username: string;
  readonly name: string;
}
