import { User } from '../enities/UserEntity';

export class CreateProjectDto {
  readonly id: number;
  readonly name: string;
  readonly description: string;
  readonly createdAt: Date;
  readonly user: User;
}
