import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('usr')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  username: string;

  @Column('varchar')
  name: string;
}
