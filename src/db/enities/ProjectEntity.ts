import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './UserEntity';

@Entity('project')
export class Project {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar')
  name: string;

  @Column('varchar')
  description: string;

  @Column()
  createdAt: Date;

  @ManyToOne(type => User, {cascade: ['update']})
  @JoinColumn({ referencedColumnName: 'id'})
  user: User;
}
