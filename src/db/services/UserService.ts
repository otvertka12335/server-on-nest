import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../enities/UserEntity';
import { CreateUserDto } from '../dtos/CreateUserDto';

@Injectable()
export class UserService {

  constructor(@InjectRepository(User)
              private readonly projectRepo: Repository<User>) {
  }

  public findAll(): Promise<User[]> {
    return this.projectRepo.find();
  }

  public findOne(id: number): Promise<User> {
    return this.projectRepo.findOne(id);
  }

  public findOneByEmail(email: string): Promise<User> {
    return this.projectRepo.findOne({where: {username: email}});
  }

  public create(createUserDto: CreateUserDto) {
    return this.projectRepo.save(createUserDto);
  }

  public edit(id: number, createUserDto: CreateUserDto) {
    this.projectRepo.update(id, createUserDto);
    return this.projectRepo.findOne(id);
  }

  public remove(id: number) {
    const deleted = this.projectRepo.findOne(id);
    this.projectRepo.delete(id);
    return deleted;
  }
}
