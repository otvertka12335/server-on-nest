import { Injectable } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { Project } from '../enities/ProjectEntity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { CreateProjectDto } from '../dtos/CreateProjectDto';
import { async } from 'rxjs/internal/scheduler/async';
import { debuglog } from 'util';

@Injectable()
export class ProjectService {

  constructor(@InjectRepository(Project)
              private readonly projectRepo: Repository<Project>) {
  }

  public findAll(): Promise<Project[]> {
    return this.projectRepo.find({
      relations: ['user'],
    });
  }

  public findAllByUser(id: number): Promise<Project[]> {
    return this.projectRepo.find({
      relations: ['user'],
      where: {
        user: id,
      },
    });
  }

  public findOne(id: number): Observable<Project> {
    return from(this.projectRepo.findOne(id, {
      relations: ['user'],
    }));
  }

  public findOneByEmail(email: string): Promise<Project> {
    return this.projectRepo.findOne({
      relations: ['user'],
    });
  }

  public create(dto: CreateProjectDto): Promise<Project> {
    return this.projectRepo.save(dto);
  }

  public edit(id: number, dto: CreateProjectDto) {
    return this.projectRepo.update(id, dto);
    // return this.projectRepo.findOne(id, {
    //   relations: ['user'],
    // });
  }

  public remove(id: number) {
    const deleted = this.projectRepo.findOne(id);
    this.projectRepo.delete(id);
    return deleted;
  }
}
