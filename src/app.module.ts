import { Module } from '@nestjs/common';
import { DataBaseModule } from './db/DataBaseModule';
import {TypeOrmModule} from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'nest',
      entities: [__dirname + '/**/*Entity{.ts,.js}'],
      // synchronize: true,
      // dropSchema: true,
    }),
    DataBaseModule,
  ],
  controllers: [
  ],
  providers: [
  ],
})
export class AppModule {
}
